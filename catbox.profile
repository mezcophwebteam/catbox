<?php
/**
 * Implements hook_form_FORM_ID_alter().
 */
function catbox_form_install_configure_form_alter(&$form, &$form_state) {
  // Pre-populate the installation configure form with some logical values
  $form['site_information']['site_name']['#default_value'] = 
    "Just Another MEZCOPH Site";
  $form['site_information']['site_mail']['#default_value'] =
    "cophoit@email.arizona.edu";
  $form['admin_account']['account']['name']['#default_value'] =
    "cophadmin";
  $form['admin_account']['account']['mail']['#default_value'] =
    "cophoit@email.arizona.edu";
  $form['server_settings']['site_default_country']['#default_value'] = "US";
  $form['server_settings']['date_default_timezone']['#default_value'] =
    "America/Phoenix";
}
